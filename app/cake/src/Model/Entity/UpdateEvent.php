<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UpdateEvent Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property string $project
 * @property string $inscription_type
 * @property string $event_comments
 *
 * @property \App\Model\Entity\Author[] $authors
 */
class UpdateEvent extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'created_by' => true,
        'project' => true,
        'inscription_type' => true,
        'event_comments' => true,
        'authors' => true
    ];
}
