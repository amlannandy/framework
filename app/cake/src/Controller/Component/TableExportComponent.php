<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;

class TableExportComponent extends Component
{
    public $components = ['RequestHandler'];

    public $types = [
        'csv' => 'text/csv',
        'tsv' => 'text/tab-separated-values',
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ];

    public function initialize(array $config)
    {
        $this->RequestHandler->setConfig([
            'viewClassMap.csv' => 'Csv',
            'viewClassMap.tsv' => 'Tsv',
            'viewClassMap.xlsx' => 'Xlsx'
        ]);
    }

    public function beforeRender(Event $event)
    {
        $type = $this->RequestHandler->prefers();
        $controller = $event->getSubject();
        $controller->response = $controller->response->withType($type);

        // override response type for CsvView
        if (isset($this->types[$type])) {
            $controller->set('_tableType', $type);
        }
    }
}
