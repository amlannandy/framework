<?php
    if(env('APP_ENV') !== 'development') { 
        $ga = new PHPGangsta_GoogleAuthenticator();
        $secret = $ga->createSecret();
        $oneCode = $ga->getCode($secret);
        echo "Checking Code '$oneCode' and Secret '$secret':\n";
        $qrCodeUrl = $ga->getQRCodeGoogleUrl('cdli.ucla.edu', $secret);
    }
?>

<hr>

<div class="container-fluid">
    <div class="row justify-content-md-center">
        <?php if(env('APP_ENV') === 'development') { ?>
            <div class="boxed col-md-6">
                <div class="capital-heading">Two Factor Authentication</div>            
                    <?= $this->Form->create() ?>    
                        <div class="form-group text-left my-4">
                            For Two-Factor Authentication (2FA) setup,
                            <br>
                            set <b>APP_ENV = "Production"</b> in <b>.env</b> and try again.
                            <br>
                            Currently you are in <b>development</b> mode.
                        </div>
                        <?= $this->Form->input('code', [
                            'type' => 'hidden',
                            'value' => 'random_code_value'
                        ]); ?>
                        <?= $this->Form->input('secretcode', [
                            'type' => 'hidden',
                            'value' => 'random_code_value'
                        ]); ?>
                        <?= $this->Form->input('checkconfirm', [
                            'type' => 'hidden',
                            'value' => '1',
                        ]); ?>
                        <?= $this->Form->submit('Skip', ['class' => 'form-control btn btn-primary col-md-2']); ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        <?php } else { ?>
        <div class="boxed col-md-6">
            <div class="capital-heading">Two Factor Authentication</div>

            <?= $this->Flash->render('auth') ?>
            <?= $this->Flash->render() ?>

            <?= $this->Form->create() ?>
                <div class="form-group text-left my-4">
                    <?= $this->Form->input('code', [
                        'type' => 'text',
                        'class' => 'form-control',
                        'label' => 'Code',
                        'required' => true
                    ]); ?>
                    <?= $this->Form->input('secretcode', [
                        'type' => 'hidden',
                        'class' => 'form-control',
                        'value' => $secret
                    ]); ?>
                </div>

                <div class="text-center my-4">
                    <?= $this->Form->input('checkconfirm', [
                        'type' => 'checkbox',
                        'class' => 'form-check-input',
                        'value' => '1',
                        'required'=>true,
                        'label'=>'I have backed up my 16-digit key.'
                    ]); ?>
                    <?= $this->Form->button('Enable 2FA', [
                        'div' => false,
                        'class' => 'btn btn-primary mt-3 signup rounded-0',
                        'title' => 'Enable 2FA'
                    ]); ?>
                </div>
            <?= $this->Form->end() ?>

        </div>

        <div class="boxed col-md text-center">
            <div> <?php echo "Secret Key is: ".$secret."\n\n"; ?> </div>
            <img src="<?php  echo $qrCodeUrl; ?>" name="qr" class="my-4" />
        </div>
        <?php } ?>
    </div>
</div>

<script language="JavaScript" type="text/javascript">
    $(document).ready(function () {
                setTimeout(function(){
                  location.reload(true);
                }, 150000);       
            });    
</script>
