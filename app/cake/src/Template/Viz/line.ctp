<div id="viz-container">
    <div class="container-fluid">
        <div id="line-chart">
            <!--Viz Goes here-->
        </div>
        
        <!--Fallback Image-->
        <noscript>
            <?= $this->Html->image('d3-fallbacks/line.png', ['class' => 'fallback-image', 'alt' => 'CDLI Line Chart']) ?>
            <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
        </noscript>
    </div>
</div>

<script type="text/javascript">
    var data = <?= $data ?>;
</script>

<?php echo $this->Html->script(['d3', 'd3-line']); ?>